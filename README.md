**# RetroFit Android Library guide #**

## Using Retrofix for HTTP Asynctask ##
1. Add Dependency at Gradle, and allow permission Internet at manifest Manifest and Gradle

2. Create Model based on the return JSON from API, use http://www.jsonschema2pojo.org/ to generate ModelJSON

3. Create class for the Base URL URL

4. Create Interface for the ENDPOINT from the Base URL API.java

5. Create the REST Service for the HTTP Call using base URL and interface and Retro. Create an interface onSuccess for calling callUsers.java

6. In activity, bind the xml and call the REST Service using the onSuccess