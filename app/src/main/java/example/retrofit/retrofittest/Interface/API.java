package example.retrofit.retrofittest.Interface;

import example.retrofit.retrofittest.Model.ModelJSON;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by addam01 on 29/02/2016.
 * 4
 */
public interface API {

    @GET("/users/basil2style")
    Call<ModelJSON> getName();


}
