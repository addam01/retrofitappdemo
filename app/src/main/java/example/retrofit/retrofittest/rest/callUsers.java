package example.retrofit.retrofittest.rest;
/**
 * 5
 */

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import example.retrofit.retrofittest.Interface.API;
import example.retrofit.retrofittest.Model.ModelJSON;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by addam01 on 29/02/2016.
 */
public class callUsers
{
    private callUserListener mcallUserListener;

    public callUsers(callUserListener listener){
        /* get eventListener for onSuccess*/
        mcallUserListener = listener;
        /*Make GSON */
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();


        /*Creating Rest Services*/
        Log.d("Log", "Creating rest services");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL.url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        API service  = retrofit.create(API.class);
        Log.d("Log", "Appending to API Adapter");
        Call<ModelJSON> trigger = service.getName();
        Log.d("Log", "Triggering");
        trigger.enqueue(new Callback<ModelJSON>() {
            @Override
            public void onResponse(Call<ModelJSON> call, Response<ModelJSON> response) {
                try{
                    String name = response.body().getName();
                    String url = response.body().getUrl();
                    Log.d("Log", "Getting response");
                    /*Pass the response to the onSuccess interface */
                    mcallUserListener.onSuccess(response);

                }catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ModelJSON> call, Throwable t) {

            }
        });
    }
    public interface callUserListener{

//    void onSuccess(String name, String url);

        void onSuccess(Response<ModelJSON> response);
    }

}
