package example.retrofit.retrofittest;
/**
 * Using Retrofix for HTTP Asynctask
 * 1. Add Dependency at Gradle, and allow permission Internet at manifest
 * Manifest and Gradle
 * 2. Create Model based on the return JSON from API, use http://www.jsonschema2pojo.org/ to generate
 *      ModelJSON
 * 3. Create class for the Base URL
 *      URL
 * 4. Create Interface for the ENDPOINT from the Base URL
 *      API.java
 * 5. Create the REST Service for the HTTP Call using base URL and interface and Retro. Create an interface onSuccess for calling
 *      callUsers.java
 *
 * 6. In activity, bind the xml and call the REST Service using the onSuccess
 *
 */
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import example.retrofit.retrofittest.Model.ModelJSON;
import example.retrofit.retrofittest.rest.callUsers;
import retrofit2.Response;

/**
 * 6
 */
public class MainActivity extends AppCompatActivity implements callUsers.callUserListener {

    TextView name1;
    TextView url1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        name1 = (TextView) findViewById(R.id.out_name);
        url1 = (TextView) findViewById(R.id.out_url);
/**
 * 6
 */
        callUsers callUser = new callUsers(this);
//        callUsers callUser = new callUsers(new callUsers.callUserListener() {
//            @Override
//            public void onSuccess(Response<ModelJSON> response) {
//                name1.setText(response.body().getName());
//                url1.setText(response.body().getUrl());
//            }
//
//            @Override
//            public void onSuccess(String name, String url) {
//                name1.setText(name);
//                url1.setText(url);
//            }
//        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onSuccess(Response<ModelJSON> response) {
        name1.setText(response.body().getName());
        url1.setText(response.body().getUrl());
    }
}
